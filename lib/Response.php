<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 07.12.17
 * Time: 10:17
 */

namespace MVC;


class Response
{

    private $_headers;

    function __construct()
    {
        $this->_headers = headers_list();
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->_headers;
    }




}