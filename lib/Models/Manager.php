<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 11.12.17
 * Time: 10:00
 */

namespace MVC\Models;

use MVC\App;


abstract class Manager
{

    protected $_table;

    protected $_sqlBuilder;

    function __construct($table)
    {
        $this->_table = $table;
        $this->_sqlBuilder = new SQLBuilder($this->_table);
    }

    abstract function fetch($stmt);

    abstract public function all();

    abstract public function getByID($id);


}