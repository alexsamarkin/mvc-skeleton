<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 12.12.17
 * Time: 11:15
 */

namespace MVC\Models;

use \MVC\App;


class UserManager extends Manager implements IModel
{

    /**
     * UserManager constructor.
     * @param $table
     */
    function __construct($table)
    {
        parent::__construct($table);
    }

    /**
     * @param $stmt
     * @return array
     */
    function fetch($stmt)
    {
        $arr = [];
        while($user = $stmt->fetch()) {
            $arr[] = $user;
        }
        return $arr;
    }

    /**
     * @return array
     */
    public function all()
    {
        $stmt = App::getConnection()->query($this->_sqlBuilder->select()->getQuery());

        return $this->fetch($stmt);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getByID($id)
    {
        $sql = $this->_sqlBuilder->select()->where("id", "=", ":id")->getQuery();
        $stmt = App::getConnection()->prepare($sql);
        $stmt->execute(["id" => $id]);

        return current($this->fetch($stmt));
    }

    /**
     * @param User $user
     * @param $password
     * @return array
     * @throws \Exception
     */
    public function add(User $user, $password)
    {
        $sql = $this->_sqlBuilder->insert([
            "login" => ":login",
            "email" => ":email",
            "name" => ":name",
            "password" => ":password"
        ])->getQuery();

        $stmt = App::getConnection()->prepare($sql);
        $stmt->execute([
            ":login" => $user->getLogin(),
            ":email" => $user->getEmail(),
            ":name" => $user->getName(),
            ":password" => md5($password)
        ]);

        return $this->fetch($stmt);
    }

    /**
     * @param $id
     * @return array
     */
    public function delete($id)
    {
        $sql = $this->_sqlBuilder->delete()->where("id" ,"=", ":id")->getQuery();
        $stmt = App::getConnection()->prepare($sql);
        $stmt->execute([":id" => $id]);
        return $this->fetch($stmt);
    }

    /**
     * @param array $data
     * @return User
     */
    public function load(Array $data)
    {
        $user = new User();
        $user->setEmail($data["email"]);
        $user->setLogin($data["login"]);
        $user->setName($data["name"]);
        $user->setId($data["id"]);

        return $user;
    }
}