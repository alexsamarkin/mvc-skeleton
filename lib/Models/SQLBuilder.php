<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 08.12.17
 * Time: 14:56
 */

namespace MVC\Models;


class SQLBuilder
{

    private $_table;

    private $_query;

    public function __construct($table)
    {
       $this->_table = $table;
    }

    public function select(Array $fields = null)
    {

        $fieldsStr = $fields ? implode(",", $fields) : "*";

        $this->_query = "SELECT ".$fieldsStr." FROM ".$this->_table;

        return $this;

    }

    public function update(Array $fields)
    {
        if(!$fields) {
            throw new \Exception("Trying to update emty row! Error!");
        }

        $values = "";
        foreach($fields as $key => $value) {
            $values .= $key."=".$value;
            if($value != $fields[count($fields) - 1]) {
               $values .= ",";
            }
        }

        $this->_query = "UPDATE ".$this->_table." SET ".$values;

        return $this;
    }

    public function delete(Array $fields = null)
    {

        $fieldsStr = $fields ? implode(",", $fields) : "";

        $this->_query = "DELETE ".$fieldsStr." FROM ".$this->_table;

        return $this;
    }



    function insert(Array $fields)
    {

        if(!$fields) {
           throw new \Exception("Trying to insert emty row! Error!");
        }

        $this->_query = "INSERT INTO ".$this->_table." (".implode(",", array_keys($fields)).") VALUES (".implode(",", $fields) .")";

        return $this;
    }

    public function where($column, $operation, $value)
    {
        $this->_query .= " WHERE $column $operation $value";

        return $this;
    }


    public function getQuery()
    {
        return $this->_query;
    }

}