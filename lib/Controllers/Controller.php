<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 07.12.17
 * Time: 12:03
 */

namespace MVC\Controllers;


use MVC\Views\ViewManager;

abstract class Controller
{

    protected $_view;

    public function __construct()
    {
        $this->_view = ViewManager::instance();
    }

}