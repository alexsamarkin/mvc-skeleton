<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 07.12.17
 * Time: 12:04
 */

namespace MVC\Controllers;

use MVC\Models\User;
use MVC\Models\UserManager;
use MVC\Request;
use MVC\Response;


class PageController extends Controller
{

    public function index(Request $request, Response $response)
    {
        $this->_view->render("index.php", ["a" => "HELLO"]);
    }

    public function about(Request $request, Response $response)
    {

    }

    public function post(Request $request, Response $response)
    {

    }

    public function item(Request $request, Response $response)
    {

    }

}