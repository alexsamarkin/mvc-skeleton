<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 06.12.17
 * Time: 17:23
 */

namespace MVC\Routing;

use MVC\App;
use MVC\Request;
use MVC\Response;

class Route {

    private $_routes = [];

    private $_request;

    /**
     * Route constructor.
     */
    public function __construct()
    {
        $this->_request = new Request();
        $this->_response = new Response();
    }

    public function start()
    {
        $uri = $this->_request->getUri();
        $method = $this->_request->getMethod();

        if(!$this->_exists($method, $uri)) {
            return $this->notFound();
        }

        $route = current($this->_getRoute($method, $uri));
        $this->_invokeCallable($route);



    }

    private function _invokeCallable(Routenode $routenode)
    {
        $routenode->getAction()->call($this->_request, $this->_response);
    }

    public function notFound()
    {
        echo "404";
        return false;
    }

    public function add($method, $uri, $callable)
    {
        if($this->_exists($method, $uri)) {
            throw new \Exception("There are two or more routes with the same URI and METHOD");
        }

        $this->_routes[] = new Routenode($method, $uri, $callable);


    }

    private function _exists($method, $uri):bool
    {
        return !empty($this->_getRoute($method,$uri));
    }

    private function _getRoute($method, $uri)
    {
        return array_filter($this->_routes, function($item) use ($method, $uri) {
            return ( $item->getUri() == $uri ) && ( $item->getMethod() == strtoupper($method) );
        });
    }


}