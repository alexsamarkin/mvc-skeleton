<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 07.12.17
 * Time: 9:19
 */

namespace MVC\Routing;

class Routenode {

    private $_method;

    private $_uri;

    private $_isDynamic;

    private $_action;

    function __construct($method, $uri, $action)
    {
       $this->_method = strtoupper($method);
       $this->_uri = $uri;
       $this->_action = $action;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->_method;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->_uri;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->_action;
    }





}