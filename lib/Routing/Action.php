<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 07.12.17
 * Time: 15:20
 */

namespace MVC\Routing;


use MVC\Request;
use MVC\Response;

class Action
{

    private $_closure;

    private $_controller;


    function __construct($action)
    {

        if (is_callable($action)) {
            $this->_closure = $action;
        }

        if (is_array($action)) {
            $this->_controller = $action;
        }

    }

    public function call(Request $request, Response $response)
    {
        $func = $this->_closure == null ?
                 [$this->_controller["object"], $this->_controller["action"]]:
                  $this->_closure;

        call_user_func($func, $request, $response);
    }


}