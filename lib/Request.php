<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 07.12.17
 * Time: 10:17
 */

namespace MVC;


class Request
{

    private $_uri;

    private $_method;

    private $_post = null;

    private $_query;

    private $_get = [];

    private $_headers;

    function __construct()
    {
       $this->_uri = $this->_isQueryString() ? substr($_SERVER["REQUEST_URI"], 0, strpos($_SERVER["REQUEST_URI"],"?")) : $_SERVER["REQUEST_URI"];
       $this->_method = $_SERVER["REQUEST_METHOD"];
       if($this->_method == "POST") {
           $this->_post = $_POST;
       }
       $this->_query = $_SERVER["QUERY_STRING"];
       $this->_headers = apache_request_headers();
       parse_str($this->_query, $this->_get);
    }

    private function _isQueryString()
    {
        return strpos($_SERVER["REQUEST_URI"],"?") !== false;
    }

    /**
     * @return array|false
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * @return mixed
     */
    public function getUri()
    {
        return $this->_uri;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->_method;
    }

    public function get()
    {
        return $this->_get;
    }

    public function post()
    {
        return $this->_post;
    }
}