<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 16.01.18
 * Time: 16:02
 */

namespace MVC\Views;


class ViewManager
{

    const DEFAULT_PATH = "templates";

    private $_path;

    private static $instance = null;

    private function __construct()
    {
        $this->_path = self::DEFAULT_PATH;
    }

    public function render($filename, Array $data)
    {
        include_once($this->_path."/".$filename);
    }

    /**
     * @param string $path
     */
    public function setPath(string $path)
    {
        $this->_path = $path;
    }



    public static function instance()
    {
        if(self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }



}