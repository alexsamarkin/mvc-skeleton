<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 06.12.17
 * Time: 17:34
 */

namespace MVC;

use \MVC\Routing\Action;
use \MVC\Routing\Route as Route;
use \MVC\Controllers\Controller as Controller;


class App {


    /**
     * @var array
     */
    private $_config = [];

    /**
     * @var Route
     */
    private $_route;

    /**
     * @var array
     */
    private $_controllers = [];

    /**
     * @var
     */
    public static $database;

    /**
     * App constructor.
     */
    public function __construct(Array $config)
    {
       $this->_config = $config ?? [];
       $this->_route = new Route();
       $this->_initDB();
    }

    /**
     *
     */
    private function _initDB()
    {
        if($this->_config["database"]) {

            $server = $this->_config["database"]["server"];
            $dbname = $this->_config["database"]["dbname"];
            $charset = $this->_config["database"]["charset"];
            $user = $this->_config["database"]["user"];
            $password = $this->_config["database"]["password"];

            $dsn = "mysql:host=$server;dbname=$dbname;charset=$charset";
            $opt = [
                \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                \PDO::ATTR_EMULATE_PREPARES   => false,
            ];
            self::$database = new \PDO($dsn, $user, $password, $opt);
        }
    }

    public static function getConnection()
    {
        return self::$database;
    }


    /**
     * @param $method
     * @return bool
     */
    private function _checkMethod($method):bool
    {
        return in_array($method, $this->_config["methods"]);
    }

    /**
     * @param $str
     * @return array
     */
    private function _parseController($str)
    {
        $arrstr = explode(":", $str);
        $classname = $arrstr[0];
        $methodname = $arrstr[1];

        $match = array_filter($this->_controllers, function($controller) use ($classname) {
            return $controller instanceof $classname;
        });

        return ["object" => current($match), "action" => $methodname ];

    }

    /**
     * @param $methodname
     * @param $uri
     * @param $callable
     * @throws \Exception
     */
    public function bind($methodname, $uri, $callable)
    {
        if(!$this->_checkMethod($methodname)) {
            throw new \Exception("Method ".$methodname." is not allowed in this app");
        }

        $callable = is_callable($callable) ? $callable : $this->_parseController($callable);

        $this->_route->add($methodname, $uri, new Action($callable));
    }

    /**
     *
     */
    public function start()
    {
       $this->_route->start();
    }

    /**
     * @param Controller $controller
     */
    public function addController(Controller $controller)
    {
       $this->_controllers[] = $controller;
    }
}