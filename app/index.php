<?php
/**
 * Created by PhpStorm.
 * User: supportauthentica
 * Date: 06.12.17
 * Time: 17:19
 */

require __DIR__."/../vendor/autoload.php";

$config = [
    "methods" => ["GET", "POST"],
    "database" => [
        "server" => "localhost",
        "user" => "root",
        "password" => "GG00At4o",
        "dbname" => "mvc",
        "charset" => "utf8"
    ],
    "views" => "/../lib/Views/templates"
];

$app = new \MVC\App($config);
$app->addController(new \MVC\Controllers\PageController());




$app->bind("GET", "/a", function(\MVC\Request $request) {
    var_dump($request);
});

$app->bind("GET", "/", "\MVC\Controllers\PageController:index");
$app->bind("GET", "/about", "\MVC\Controllers\PageController:about");
$app->bind("POST", "/post", "\MVC\Controllers\PageController:post");
$app->bind("GET", "/item/{id}", "\MVC\Controllers\PageController:get");



$app->start();